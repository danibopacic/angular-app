import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { OrdersService } from '../orders.service';
import { Order } from '../order.interface';

import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { MatDialog } from '@angular/material/dialog';
import { OrderDetailsComponent } from '../order-details/order-details.component';
import { Table } from '../table.interface';
import { Observable, Subscription, timer } from 'rxjs';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css'],
})
export class OrderComponent implements OnInit {
  tables: Table[] = [];
  orders_in_progress: Table[] = [];
  completed_orders: Table[] = [];

  selectedOrder: Order | null = null;

  @ViewChild('todoList') todoList: any;
  @ViewChild('doneList') doneList: any;

  //private interval: any;

  subscription!: Subscription;
  everyTenSeconds: Observable<number> = timer(0, 10000);

  showDetails(order: Table): void {
    const dialogRef = this.dialog.open(OrderDetailsComponent, {
      data: { order: order },
      minWidth: '300px',
    });

    dialogRef.componentInstance.markAsDoneEvent.subscribe((id: number) => {
      this.markAsDone(id);
    });
  }

  constructor(
    private ordersService: OrdersService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.getTables();
    this.getReadyOrders();

    //this.subscription = this.everyTenSeconds.subscribe(() => {
    //  this.getTables();
    //  this.getReadyOrders();
    //});
  }

  //ngOnDestroy() {
  //  this.subscription.unsubscribe();
  //}

  refreshOrders(): void {
    this.getTables();
    this.getReadyOrders();
  }

  getTables(): void {
    this.ordersService.getTables().subscribe((data) => {
      this.tables = data
        .filter((x) => x.status.toLowerCase() == 'new')
        .map((row: Table) => {
          return {
            id: row.id,
            myTable: row.myTable,
            comment: row.comment,
            status: row.status,
          };
        });
      console.log(this.tables);
    });
  }

  getReadyOrders(): void {
    this.ordersService.getTables().subscribe((data) => {
      this.orders_in_progress = data
        .filter((x) => x.status.toLowerCase() === 'ready' && x.myTable !== null)
        .map((row: Table) => {
          return {
            id: row.id,
            myTable: row.myTable,
            comment: row.comment,
            status: row.status,
          };
        });
      console.log(this.orders_in_progress);
    });
  }

  closeDetails(): void {
    this.selectedOrder = null;
  }

  drop(event: CdkDragDrop<Table[]>) {
    if (
      event.previousContainer === this.todoList &&
      event.container === this.doneList
    ) {
      const movedItem = event.previousContainer.data[event.previousIndex];
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
      this.updateOrderStatus(movedItem, 'READY');
    }

    console.log(event);
    console.log(this.orders_in_progress);
  }

  updateOrderStatus(table: Table, status: string) {
    this.ordersService.updateOrderStatus(table, status).subscribe(
      () => {
        console.log('Status successfully updated.');
      },
      (error) => {
        console.error('Error updating status:', error);
      }
    );
  }

  markAsDone(id: number): void {
    const order = this.orders_in_progress.find((order) => order.id === id);
    if (order) {
      this.updateOrderStatus(order, 'completed');
      this.completed_orders.push(order);
      const index = this.orders_in_progress.indexOf(order);
      if (index !== -1) {
        this.orders_in_progress.splice(index, 1);
      }
    }
  }
}
