import { Component } from '@angular/core';

@Component({
  selector: 'app-completed-orders',
  templateUrl: './completed-orders-screen.component.html',
  styleUrl: './completed-orders-screen.component.css',
})
export class CompletedOrdersComponentScreen {}
