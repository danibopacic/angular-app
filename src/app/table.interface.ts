export interface Table {
  id: number;
  myTable: Subtable;
  comment: string;
  status: string;
}

export interface Subtable {
  id: number;
  name: string;
}
