export interface Order {
  drinkId: number;
  quantity: number;
  name: string;
  price: number;
}
