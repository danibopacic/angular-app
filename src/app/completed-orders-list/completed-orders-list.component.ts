import { Component, Input, OnInit } from '@angular/core';
import { Order } from '../order.interface';
import { OrderDetailsComponent } from '../order-details/order-details.component';
import { MatDialog } from '@angular/material/dialog';
import { Table } from '../table.interface';
import { OrdersService } from '../orders.service';

@Component({
  selector: 'app-completed-orders-list',
  templateUrl: './completed-orders-list.component.html',
  styleUrls: ['./completed-orders-list.component.css'],
})
export class CompletedOrdersListComponent implements OnInit {
  completed_orders: Table[] = [];
  orderDetails: Order[] = [];
  totalPrice = 0;

  constructor(
    private dialog: MatDialog,
    private ordersService: OrdersService
  ) {}

  ngOnInit(): void {
    this.getCompletedOrders();
    console.log(this.completed_orders);
  }

  getCompletedOrders(): void {
    this.ordersService.getTables().subscribe((data) => {
      this.completed_orders = data
        .filter(
          (x) => x.status.toLowerCase() === 'completed' && x.myTable !== null
        )
        .map((row: Table) => {
          return {
            id: row.id,
            myTable: row.myTable,
            comment: row.comment,
            status: row.status,
          };
        });
      console.log(this.completed_orders);
      this.calculateTotalPriceForEachOrder();
    });
  }

  calculateTotalPrice(orderDetails: Order[]): number {
    return orderDetails.reduce(
      (total, order) => total + order.quantity * order.price,
      0
    );
  }

  calculateTotalPriceForEachOrder(): void {
    this.completed_orders.forEach((order) => {
      this.fetchOrderItems(order.id);
    });
  }

  fetchOrderItems(orderId: number): void {
    this.ordersService.getOrderDetails(orderId).subscribe((items) => {
      const totalPriceForOrder = this.calculateTotalPrice(items);
      this.totalPrice += totalPriceForOrder;
    });
  }

  showDetails(order: Table): void {
    const dialogRef = this.dialog.open(OrderDetailsComponent, {
      data: { order: order },
      minWidth: '300px',
    });
  }
}
