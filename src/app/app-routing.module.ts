import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompletedOrdersComponentScreen } from './screens/completed-orders-screen/completed-orders-screen.component';
import { HomeComponent } from './screens/home-screen/home.component';
import { HttpClientModule } from '@angular/common/http';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'completed-orders', component: CompletedOrdersComponentScreen },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), HttpClientModule],
  exports: [RouterModule],
})
export class AppRoutingModule {}
