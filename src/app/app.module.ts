import { NgModule } from '@angular/core';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OrderComponent } from './order/order.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { NavComponent } from './nav/nav.component';
import { FooterComponent } from './footer/footer.component';
import { CompletedOrdersComponentScreen } from './screens/completed-orders-screen/completed-orders-screen.component';
import { HomeComponent } from './screens/home-screen/home.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { CompletedOrdersListComponent } from './completed-orders-list/completed-orders-list.component';
import { MatDialogModule } from '@angular/material/dialog';
import {
  HttpClientModule,
  provideHttpClient,
  withFetch,
} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    OrderComponent,
    OrderDetailsComponent,
    NavComponent,
    FooterComponent,
    CompletedOrdersComponentScreen,
    HomeComponent,
    ConfirmationDialogComponent,
    CompletedOrdersListComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatDialogModule,
    DragDropModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
  ],
  providers: [provideHttpClient(withFetch())],
  bootstrap: [AppComponent],
})
export class AppModule {}
