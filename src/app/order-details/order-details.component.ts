import { Component, EventEmitter, Inject, Input, Output } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Order } from '../order.interface';
import { OrdersService } from '../orders.service';
import { Table } from '../table.interface';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css'],
})
export class OrderDetailsComponent {
  @Output() markAsDoneEvent = new EventEmitter<number>();
  @Input() in_progress: Table[] = [];
  orderDetails: Order[] = [];

  constructor(
    public dialogRef: MatDialogRef<OrderDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ordersService: OrdersService
  ) {
    this.fetchOrderItems(data.order.id);
  }

  fetchOrderItems(orderId: number): void {
    this.ordersService.getOrderDetails(orderId).subscribe((items) => {
      this.orderDetails = items;
    });
  }

  calculateTotalPrice(orderDetails: Order[]): number {
    return orderDetails.reduce(
      (total, order) => total + order.quantity * order.price,
      0
    );
  }

  markAsDone(id: number): void {
    this.markAsDoneEvent.emit(id);
    this.dialogRef.close();
  }
}
