import { Injectable } from '@angular/core';
import { Observable, catchError, from, of, throwError } from 'rxjs';
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { environment } from '../environments/environment.development';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Table } from './table.interface';
import { Order } from './order.interface';

@Injectable({
  providedIn: 'root',
})
export class OrdersService {
  constructor(private http: HttpClient) {}

  private apiUrl = 'http://10.15.207.161:8080';

  getTables(): Observable<Table[]> {
    const url = `${this.apiUrl}/myorder/getAll`;
    return new Observable((observer) => {
      axios
        .get(url)
        .then((response) => {
          observer.next(response.data);
          observer.complete();
        })
        .catch((error) => {
          observer.error(error);
        });
    });
  }

  getOrderDetails(tableId: number): Observable<Order[]> {
    const url = `${this.apiUrl}/myorderdrink/getOrderedDrinks/${tableId}`;
    return new Observable((observer) => {
      axios
        .get(url)
        .then((response) => {
          observer.next(response.data);
          observer.complete();
        })
        .catch((error) => {
          observer.error(error);
        });
    });
  }

  updateOrderStatus(table: Table, status: string): Observable<void> {
    const url = `${this.apiUrl}/myorder/${table.id}`;
    return new Observable((observer) => {
      axios
        .put(url, { ...table, status })
        .then(() => {
          observer.next();
          observer.complete();
        })
        .catch((error) => {
          observer.error(error);
        });
    });
  }
}
