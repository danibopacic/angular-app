import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NavComponent } from './nav.component';

describe('NavComponent', () => {
  let component: NavComponent;
  let fixture: ComponentFixture<NavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NavComponent],
      imports: [RouterTestingModule],
    }).compileComponents();

    fixture = TestBed.createComponent(NavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain "Orders" link', () => {
    const compiled = fixture.nativeElement;
    const link = compiled.querySelector('a[href="/"]');
    expect(link).toBeTruthy();
    expect(link.textContent.trim()).toBe('Orders');
  });

  it('should contain "Completed orders" link', () => {
    const compiled = fixture.nativeElement;
    const link = compiled.querySelector('a[href="/completed-orders"]');
    expect(link).toBeTruthy();
    expect(link.textContent.trim()).toBe('Completed orders');
  });
});
